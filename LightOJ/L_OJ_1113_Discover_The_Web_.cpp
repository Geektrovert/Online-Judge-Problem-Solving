
#include <algorithm>
#include <bitset>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <deque>
#include <functional>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <limits>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#define fin() freopen("in.c","r",stdin)
#define fout() freopen("out.c","w",stdout)
#define iint(x) scanf("%d",&x)
#define pint(x) printf("%d",x)
#define istr(x) scanf("%s",x)
#define iln(x) scanf("%[^\n],x")
#define pstr(x) printf("%s",x)
#define garbage() getch()
#define line() printf("\n")
#define cases(cs,T) for(cs=1;cs<=T;cs++)
#define caseout(cs) printf("Case %d:",cs)
#define space() printf(" ")
#define pb(x) push_back(x)

using namespace std;

int main()
{
    #if 0
    fin();
    fout();
    #endif
    ///WRITE CODE BELOW
    int T,cs;
    iint(T);
    cases(cs,T)
    {
        int pos=1;
        vector<string> sites;
        sites.pb("http://www.lightoj.com/");
        string s,site;
        caseout(cs);
        line();
        while(cin>>s)
        {
            if(s=="QUIT") break;
            else if(s=="VISIT")
            {
                cin >> site;
                if(sites.size()!=1) sites.erase(sites.begin()+pos,sites.end());
                sites.pb(site);
                cout << site << endl;
                pos=sites.size();
            }
            else if(s=="BACK")
            {
                if(pos==1)
                {
                    cout << "Ignored" << endl;
                    continue;
                }
                pos--;
                cout << sites[pos-1] << endl;
            }
            else
            {
                if(pos==sites.size())
                {
                    cout << "Ignored" << endl;
                    continue;
                }
                pos++;
                cout << sites[pos-1] << endl;
            }
        }
    }
}
