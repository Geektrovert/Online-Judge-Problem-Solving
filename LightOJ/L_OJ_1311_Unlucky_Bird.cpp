#include <algorithm>
#include <bitset>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <deque>
#include <functional>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <limits>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#define FIN() freopen("in.c","r",stdin)
#define FOUT() freopen("out.c","w",stdout)
#define IINT(x) scanf("%d",&x)
#define ID10(x) scanf("%lf",&x)
#define ILLD(x) scanf("%lld",&x)
#define ICH(x) scanf("%c",&x)
#define ISTR(x) scanf("%s",x)
#define ILN(x) scanf("%[^\n],x")
#define PINT(x) printf("%d",x)
#define PD6(x) printf("%.6lf",x)
#define PD10(x) printf("%.10lf",x)
#define PLLD(x) printf("%lld",x)
#define PSTR(x) printf("%s",x)
#define GARBAGE() getchar()
#define LINE() printf("\n")
#define LOOP(i,ini,fini) for(int i=ini; i<fini; i++)
#define CASES(cs,T) for(int cs=1;cs<=T;cs++)
#define CASEOUT(cs) printf("Case %d:",cs)
#define SPACE() printf(" ")
#define PB(x) push_back(x)
#define MEM(a,b) memset(a,b,sizeof(a))

#define EPS 1e-9
#define PI 2*acos(0.0)
#define MOD 1000000007

using namespace std;

int main()
{
#if 0
    FIN();
    FOUT();
#endif
    ///WRITE CODE BELOW
    int T;
    IINT(T);
    CASES(cs,T)
    {
        double v1,v2,v3,a1,a2;
        ID10(v1),ID10(v2),ID10(v3),ID10(a1),ID10(a2);
        double t1=v1/a1,t2=v2/a2;
        double fly=v3*max(t1,t2);
        double d=v1*v1/(2.0*a1)+v2*v2/(2.0*a2);
        CASEOUT(cs), SPACE(), PD10(d), SPACE(),PD10(fly),LINE();
    }
}
