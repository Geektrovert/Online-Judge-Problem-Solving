#include <iostream>
#include <string>

using namespace std;

int main()
{
	int T;
	cin >> T;
	for(int cs=1; cs<=T; cs++)
	{
		long long x;
		string s;
		cin >> x >> s;
		if(s[0]=='A')
		{
			if(x%3==1) cout << "Case " << cs << ": " << "Bob" << endl;
			else cout << "Case " << cs << ": " << "Alice" << endl;
		}
		if(s[0]=='B')
		{
			if(x%3) cout << "Case " << cs << ": " << "Bob" << endl;
			else cout << "Case " << cs << ": " << "Alice" << endl;
		}
	}
}
