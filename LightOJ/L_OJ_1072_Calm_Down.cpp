#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdio>
#define pi acos(0.0)*2

using namespace std;

int main()
{
    //freopen("in.c","r",stdin);
    //freopen("out.c","w",stdout);
    int T;
    cin >> T;
    for(int cs=1; cs<=T; cs++)
    {
        double R,n;
        cin >> R >> n;
        printf("Case %d: %.6lf\n",cs,(sin(pi/n)*R)/(sin(pi/n)+1));
    }
}
