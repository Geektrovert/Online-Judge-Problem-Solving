
#include <algorithm>
#include <bitset>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <deque>
#include <functional>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <limits>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#define fin() freopen("in.c","r",stdin)
#define fout() freopen("out.c","w",stdout)
#define iint(x) scanf("%d",&x)
#define pint(x) printf("%d",x)
#define istr(x) scanf("%s",x)
#define iln(x) scanf("%[^\n],x")
#define pstr(x) printf("%s",x)
#define garbage() getch()
#define line() printf("\n")
#define cases(cs,T) for(cs=1;cs<=T;cs++)
#define caseout(cs) printf("Case %d:",cs)
#define space() printf(" ")
#define pb(x) push_back(x)

#define EPS 1e-9
#define PI 2*acos(0.0)
#define MOD 1000000007

int ones(int n)
{
    int cnt=0;
    while(n)
    {
        n&=(n-1);
        cnt++;
    }
    return cnt;
}

using namespace std;

int main()
{
    #if 0
    fin();
    fout();
    #endif
    ///WRITE CODE BELOW
    int T,cs;
    iint(T);
    cases(cs,T)
    {
        caseout(cs);
        space();
        int n;
        iint(n);
        if(ones(n)&1) cout << "odd" << endl;
        else cout << "even" << endl;
    }
}
