#include <algorithm>
#include <bitset>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <deque>
#include <functional>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <limits>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#define FIN() freopen("in.c","r",stdin)
#define FOUT() freopen("out.c","w",stdout)
#define IINT(x) scanf("%d",&x)
#define ID10(x) scanf("%lf",&x)
#define ILLD(x) scanf("%lld",&x)
#define ICH(x) scanf("%c",&x)
#define ISTR(x) scanf("%s",x)
#define ILN(x) scanf("%[^\n],x")
#define PINT(x) printf("%d",x)
#define PD6(x) printf("%.6lf",x)
#define PD10(x) printf("%.10lf",x)
#define PLLD(x) printf("%lld",x)
#define PSTR(x) printf("%s",x)
#define GARBAGE() getchar()
#define LINE() printf("\n")
#define LOOP(i,ini,fini) for(int i=ini; i<fini; i++)
#define CASES(cs,T) for(int cs=1;cs<=T;cs++)
#define CASEOUT(cs) printf("Case %d:",cs)
#define SPACE() printf(" ")
#define PB(x) push_back(x)
#define MEM(a,b) memset(a,b,sizeof(a))

#define EPS 1e-9
#define PI 2*acos(0.0)
#define MOD 1000000007

using namespace std;

bool find_thing(int things[],int n,int thing)
{
    bool flag=false;
    LOOP(i,0,n)
    {
        if(things[i]==thing)
        {
            flag=true;
            break;
        }
    }
    return flag;
}

int main()
{
    #if 0
    FIN();
    FOUT();
    #endif
    ///WRITE CODE BELOW
    int T;
    IINT(T);
    CASES(cs,T)
    {
        int n,m,k,q;
        IINT(n);
        IINT(m);
        IINT(k);
        int arr[n][k];
        LOOP(i,0,n)
            LOOP(j,0,k)
                IINT(arr[i][j]);
        IINT(q);
        int sol[q];
        LOOP(i,0,q)
            IINT(sol[i]);
        int i;
        for(i=0; i<n; i++)
        {
            int j;
            for(j=0; j<k; j++)
            {
                if(arr[i][j]>0 && find_thing(sol,q,abs(arr[i][j]))) break;
                else if (arr[i][j]<0 && !find_thing(sol,q,abs(arr[i][j]))) break;
            }
            if(j==k) break;
        }
        CASEOUT(cs);
        SPACE();
        if(i==n) printf("Yes");
        else printf("No");
        LINE();
    }
}
