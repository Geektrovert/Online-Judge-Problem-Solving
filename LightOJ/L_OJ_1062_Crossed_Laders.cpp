#include <algorithm>
#include <bitset>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <deque>
#include <functional>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <limits>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#define FIN() freopen("in.c","r",stdin)
#define FOUT() freopen("out.c","w",stdout)
#define IINT(x) scanf("%d",&x)
#define ID10(x) scanf("%lf",&x)
#define ILLD(x) scanf("%lld",&x)
#define ICH(x) scanf("%c",&x)
#define ISTR(x) scanf("%s",x)
#define ILN(x) scanf("%[^\n],x")
#define PINT(x) printf("%d",x)
#define PD6(x) printf("%.6lf",x)
#define PD10(x) printf("%.10lf",x)
#define PLLD(x) printf("%lld",x)
#define PSTR(x) printf("%s",x)
#define GARBAGE() getchar()
#define LINE() printf("\n")
#define LOOP(i,ini,fini) for(int i=ini; i<fini; i++)
#define CASES(cs,T) for(int cs=1;cs<=T;cs++)
#define CASEOUT(cs) printf("Case %d:",cs)
#define SPACE() printf(" ")
#define PB(x) push_back(x)
#define MEM(a,b) memset(a,b,sizeof(a))

#define EPS 1e-9
#define PI 2*acos(0.0)
#define MOD 1000000007

using namespace std;

double ucchota(double otibhuj, double bhumi)
{
    return sqrt(otibhuj*otibhuj-bhumi*bhumi);
}

double baal(double a, double b)
{
    double d=a*b/(a+b);
    return d;
}

int main()
{
    #if 0
    FIN();
    FOUT();
    #endif
    ///WRITE CODE BELOW
    int T;
    IINT(T);
    CASES(cs,T)
    {
        double x,y,a,b,c,d;
        ID10(x);
        ID10(y);
        ID10(c);
        double hi=min(x,y),lo=0.0,mid=(hi+lo)/2.0;
        while(1)
        {
            mid=(hi+lo)/2.0;
            a=ucchota(x,mid);
            b=ucchota(y,mid);
            d=baal(a,b);
            //cout << mid << endl;
            //cout << "---" << a << "---" << b << "---" << endl;
            //cout << "***" << c << "***" << d << "***" << endl;
            if(abs(c-d)<EPS)
            {
                break;
            }
            if(d>c) lo=mid;
            else hi=mid;
        }
        CASEOUT(cs);
        SPACE();
        PD10(mid);
        LINE();
    }
}
