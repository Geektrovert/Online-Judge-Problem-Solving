#include <algorithm>
#include <bitset>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <deque>
#include <functional>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <limits>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#define FIN() freopen("in.c","r",stdin)
#define FOUT() freopen("out.c","w",stdout)
#define IINT(x) scanf("%d",&x)
#define ID10(x) scanf("%lf",&x)
#define ILLD(x) scanf("%lld",&x)
#define ICH(x) scanf("%c",&x)
#define ISTR(x) scanf("%s",x)
#define ILN(x) scanf("%[^\n],x")
#define PINT(x) printf("%d",x)
#define PD6(x) printf("%.6lf",x)
#define PD10(x) printf("%.10lf",x)
#define PLLD(x) printf("%lld",x)
#define PSTR(x) printf("%s",x)
#define GARBAGE() getchar()
#define LINE() printf("\n")
#define LOOP(i,ini,fini) for(int i=ini; i<fini; i++)
#define CASES(cs,T) for(int cs=1;cs<=T;cs++)
#define CASEOUT(cs) printf("Case %d:",cs)
#define SPACE() printf(" ")
#define PB(x) push_back(x)
#define MEM(a,b) memset(a,b,sizeof(a))

#define EPS 1e-9
#define PI 2*acos(0.0)
#define MOD 1000000007

using namespace std;

int main()
{
#if 0
    FIN();
    FOUT();
#endif
    ///WRITE CODE BELOW
    int T;
    IINT(T);
    CASES(cs,T)
    {
        double x,y,z;
        ID10(x),ID10(y),ID10(z);
        double a=y+z,b=x+z,c=x+y;
        //cout << a << "  <<--->>  " << b << "  <<--->>  " << c << endl;
        double A=(b*b+c*c-a*a)/(2.0*b*c),B=(c*c+a*a-b*b)/(2.0*c*a),C=(a*a+b*b-c*c)/(2.0*a*b);
        (A+1<EPS)?A=x*x*PI:A=x*x*acos(A);
        (B+1<EPS)?B=y*y*PI:B=y*y*acos(B);
        (C+1<EPS)?C=z*z*PI:C=z*z*acos(C);
        double area=(a+b+c)/2.0;
        area=sqrt(area*(area-a)*(area-b)*(area-c));
        //cout << area << endl;
        area=area-A/2.0-B/2.0-C/2.0;
        CASEOUT(cs),SPACE(),PD6(area),LINE();
        //cout << A/2.0 << "  <<--->>  " << B/2.0 << "  <<--->>  " << C/2.0 << endl;
    }
}
