#include <algorithm>
#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <iterator>

#define iint(x) scanf("%d",&x)
#define pint(x) printf("%d",x)
#define line() printf("\n")
#define cases(cs,T) for(cs=1;cs<=T;cs++)
#define caseout(cs) printf("Case %d: ",cs)

using namespace std;

int main()
{
    int T,cs;
    iint(T);
    cases(cs, T)
    {
        int a,b;
        iint(a),iint(b);
        a=(a+abs(a-b))*4+19;
        caseout(cs);
        pint(a);
        line();
    }
}
