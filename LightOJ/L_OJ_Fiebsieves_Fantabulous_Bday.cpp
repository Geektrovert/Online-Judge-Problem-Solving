#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	int T;
	cin >> T;
	for(int cs=1; cs<=T; cs++)
	{
		long long n,root,x,y,d;
		cin >> n;
		root=ceil(sqrt(n));
		d=root*root-n;
		if(d==0)
		{
			if(root%2) x=1,y=root;
			else x=root, y=1;
		}
		else if(root%2)
		{
		    if(d<root) x=d+1,y=root;
		    else x=root,y=2*root-d-1;
		}
		else
		{
		    if(d<root) y=d+1,x=root;
		    else y=root,x=2*root-d-1;
		}
		cout << "Case " << cs << ": " << x << " " << y << endl;
	}
}
