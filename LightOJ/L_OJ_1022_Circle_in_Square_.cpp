#include<iostream>
#include<cmath>
#include<cstdio>
#define PI 2*acos(0.0)

using namespace std;

int main()
{
    int T;
    cin >> T;
    for(int cs=1; cs<=T; cs++)
    {
        double n;
        cin >> n;
        cout << "Case " << cs << ": ";
        printf("%.2lf\n",(4-PI)*n*n);
    }
}
