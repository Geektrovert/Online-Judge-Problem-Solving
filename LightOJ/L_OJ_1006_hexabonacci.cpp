#include<iostream>
#include<cstdio>
#include<cstring>

#define MOD 10000007
using namespace std;

int a, b, c, d, e, f;

int dp[100000 + 5];
int fn( int n ) {
    if(dp[n] != -1)
        return dp[n];
    if( n == 0 ) return a;
    if( n == 1 ) return b;
    if( n == 2 ) return c;
    if( n == 3 ) return d;
    if( n == 4 ) return e;
    if( n == 5 ) return f;
    return dp[n] = ( fn(n-1)%MOD + fn(n-2)%MOD + fn(n-3)%MOD + fn(n-4)%MOD + fn(n-5)%MOD + fn(n-6)%MOD );
}
int main() {
    int n, caseno = 0, cases;
    scanf("%d", &cases);
    while( cases-- ) {
        memset(dp, -1, sizeof(dp));
        scanf("%d %d %d %d %d %d %d", &a, &b, &c, &d, &e, &f, &n);
        printf("Case %d: %d\n", ++caseno, fn(n)%MOD);
    }
    return 0;
}
