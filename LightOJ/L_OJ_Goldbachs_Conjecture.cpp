#include <iostream>
#include <cstring>
#include <vector>
#define pr_size 10000005

using namespace std;

bool prime[pr_size];
vector<int> pprime;
void seive()
{
	for(int i=0; i<pr_size-5; i++)
        prime[i]=false;
	for(long long i=2; i<pr_size-5; i++)
	{
		if(!prime[i])
		{
		    //cout << i << endl;
		    pprime.push_back(i);
			for(long long j=i*i; j<pr_size-5; j+=i)
				prime[j]=true;
		}
	}
}

int main()
{
	seive();
	int T;
	cin >> T;
	//cout << pprime.size() << endl;
	for(int cs=1; cs<=T; cs++)
	{
		int n, cnt=0;
		cin >> n;
		for(int i=0; pprime[i]*2<=n; i++)
		{
			if(!prime[n-pprime[i]]) cnt++;
		}
		cout << "Case " << cs << ": " << cnt << endl;
	}
}
