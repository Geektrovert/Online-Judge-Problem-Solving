#include<iostream>
#include<cstdio>
#include<string>

using namespace std;

int main()
{
    //freopen("in.c","r",stdin);
    //freopen("out.c","w",stdout);
    int T;
    scanf("%d",&T);
    for(int cs=1; cs <=T; cs++)
    {
        long long n;
        scanf("%lld",&n);
        if(!(n&(n-1)))
        {
            printf("Case %d: %lld\n",cs,n<<1);
        }
        else
        {
            int target=1,cnt=0;
            while(!(n&target)) target<<=1;
            while(n&target) n&=(~target),cnt++,target<<=1;
            n|=target;
            target=1;
            cnt--;
            while(cnt--) n|=target,target<<=1;
            printf("Case %d: %lld\n",cs,n);
        }
    }
}
