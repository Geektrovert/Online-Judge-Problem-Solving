#include <algorithm>
#include <bitset>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <deque>
#include <functional>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <limits>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#define FIN() freopen("in.c","r",stdin)
#define FOUT() freopen("out.c","w",stdout)
#define IINT(x) scanf("%d",&x)
#define ILLD(x) scanf("%lld",&x)
#define ICH(x) scanf("%c",&x)
#define ISTR(x) scanf("%s",x)
#define ILN(x) scanf("%[^\n],x")
#define PINT(x) printf("%d",x)
#define PLLD(x) printf("%lld",x)
#define PSTR(x) printf("%s",x)
#define GARBAGE() getchar()
#define LINE() printf("\n")
#define CASES(cs,T) for(int cs=1;cs<=T;cs++)
#define CASEOUT(cs) printf("Case %d:",cs)
#define SPACE() printf(" ")
#define PB(x) push_back(x)
#define MEM(a,b) memset(a,b,sizeof(a))

#define EPS 1e-9
#define PI 2*acos(0.0)
#define MOD 1000000007

using namespace std;

int main()
{
    #if 0
    FIN();
    FOUT();
    #endif
    ///WRITE CODE BELOW
    int T;
    IINT(T);
    CASES(cs,T)
    {
        int n,m;
        IINT(n);
        IINT(m);
        int arr[n];
        CASES(i,n) IINT(arr[i-1]);
        CASES(i,m)
        {
            char c;
            int x,y;
            ICH(c);///GARBAGE
            ICH(c);
            if(c=='S')
            {
                IINT(x);
                CASES(j,n)
                    arr[j-1]+=x;
            }
            else if(c=='M')
            {
                IINT(x);
                CASES(j,n)
                    arr[j-1]*=x;
            }
            else if(c=='D')
            {
                IINT(x);
                CASES(j,n)
                    arr[j-1]/=x;
            }
            else if(c=='R')
            {
                CASES(j,n/2)
                    swap(arr[j-1],arr[n-j]);
            }
            else if(c=='P') IINT(x),IINT(y),swap(arr[x],arr[y]);
        }
        CASEOUT(cs);
        LINE();
        PINT(arr[0]);
        for(int i=1; i<n; i++) SPACE(),PINT(arr[i]);
        LINE();
    }
}
