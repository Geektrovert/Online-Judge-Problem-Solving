#include <algorithm>
#include <bitset>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <deque>
#include <functional>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <limits>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#define FIN() freopen("in.c","r",stdin)
#define FOUT() freopen("out.c","w",stdout)
#define IINT(x) scanf("%d",&x)
#define ILLD(x) scanf("%lld",&x)
#define ICH(x) scanf("%c",&x)
#define ISTR(x) scanf("%s",x)
#define ILN(x) scanf("%[^\n],x")
#define PINT(x) printf("%d",x)
#define PLLD(x) printf("%lld",x)
#define PSTR(x) printf("%s",x)
#define GARBAGE() getchar()
#define LINE() printf("\n")
#define CASES(cs,T) for(int cs=1;cs<=T;cs++)
#define CASEOUT(cs) printf("Case %d:",cs)
#define SPACE() printf(" ")
#define PB(x) push_back(x)
#define MEM(a,b) memset(a,b,sizeof(a))

#define EPS 1e-9
#define PI 2*acos(0.0)
#define MOD 1000000007

using namespace std;

int main()
{
    #if 0
    fin();
    fout();
    #endif
    int T,cs;
    IINT(T);
    CASES(cs,T)
    {
        CASEOUT(cs);
        LINE();
        int x1,y1,x2,y2,n;
        IINT(x1);
        IINT(y1);
        IINT(x2);
        IINT(y2);
        IINT(n);
        while(n--)
        {
            int x,y;
            IINT(x);
            IINT(y);
            if(x>=x1 && x<=x2 && y>=y1 && y<=y2) printf("Yes\n");
            else printf("No\n");
        }
    }
}

