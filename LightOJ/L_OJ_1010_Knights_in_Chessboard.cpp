    #include <stdio.h>
    int main()
    {
        int tst, i;
        int m, n;
        scanf("%d", &tst);
        for (i = 1; i <= tst; i++)
        {
            scanf("%d%d", &m, &n);
            if (m == 1 || n == 1)
                n = m*n;
            else if (m == 2 || n == 2)
            {
                if (m == 2)
                    m = n;
                if (m % 4 == 1)
                    n = m + 1;
                else if (m % 4 == 2)
                    n = m + 2;
                else if (m % 4 == 3)
                    n = m + 1;
                else if (m % 4 == 0)
                    n = m;
            }
            else if ((m*n) % 2 == 0)
                n = (m*n )<<1;
            else
                n = (m*n) <<1 + 1;
            printf("Case %d: %d\n", i, n);
        }
        return 0;
    }
