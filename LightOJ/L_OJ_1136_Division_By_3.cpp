
#include <algorithm>
#include <bitset>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <deque>
#include <functional>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <limits>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#define FIN() freopen("in.c","r",stdin)
#define FOUT() freopen("out.c","w",stdout)
#define IINT(x) scanf("%d",&x)
#define ILLD(x) scanf("%lld",&x)
#define ICH(x) scanf("%c",&x)
#define ISTR(x) scanf("%s",x)
#define ILN(x) scanf("%[^\n],x")
#define PINT(x) printf("%d",x)
#define PLLD(x) printf("%lld",x)
#define PSTR(x) printf("%s",x)
#define GARBAGE() getch()
#define LINE() printf("\n")
#define CASES(cs,T) for(int cs=1;cs<=T;cs++)
#define CASEOUT(cs) printf("Case %d:",cs)
#define SPACE() printf(" ")
#define PB(x) push_back(x)
#define MEM(a,b) memset(a,b,sizeof(a))

#define EPS 1e-9
#define PI 2*acos(0.0)
#define MOD 1000000007

using namespace std;

int main()
{
    #if 0
    FIN();
    FOUT();
    #endif
    ///WRITE CODE BELOW
    int T;
    IINT(T);
    CASES(cs,T)
    {
        CASEOUT(cs);
        SPACE();
        long long a,b,c;
        ILLD(a);
        ILLD(b);
        if(a%3==0 && b%3==0) c=(long long int)(b/3)-(long long int)(a/3)+1, c<<=1,c--;
        else if(a%3==0 && b%3==1) c=(long long int)(b/3)-(long long int)(a/3)+1, c<<=1,c--;
        else if(a%3==0 && b%3==2) c=(long long int)(b/3)-(long long int)(a/3)+1, c<<=1;
        else if(a%3==1 && b%3==0) c=(long long int)(b/3)-(long long int)(a/3), c<<=1;
        else if(a%3==2 && b%3==0) c=(long long int)(b/3)-(long long int)(a/3), c<<=1;
        else if(a%3==1 && b%3==1) c=(long long int)(b/3)-(long long int)(a/3), c<<=1;
        else if(a%3==1 && b%3==2) c=(long long int)(b/3)-(long long int)(a/3), c<<=1,c++;
        else if(a%3==2 && b%3==1) c=(long long int)(b/3)-(long long int)(a/3), c<<=1;
        else if(a%3==2 && b%3==2) c=(long long int)(b/3)-(long long int)(a/3), c<<=1,c++;
        PLLD(c);
        LINE();
    }
}
