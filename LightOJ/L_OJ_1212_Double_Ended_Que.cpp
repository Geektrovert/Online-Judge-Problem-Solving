#include <algorithm>
#include <bitset>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <deque>
#include <functional>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <limits>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#define FIN() freopen("in.c","r",stdin)
#define FOUT() freopen("out.c","w",stdout)
#define IINT(x) scanf("%d",&x)
#define ID10(x) scanf("%lf",&x)
#define ILLD(x) scanf("%lld",&x)
#define ICH(x) scanf("%c",&x)
#define ISTR(x) scanf("%s",x)
#define ILN(x) scanf("%[^\n],x")
#define PINT(x) printf("%d",x)
#define PD6(x) printf("%.6lf",x)
#define PD10(x) printf("%.10lf",x)
#define PLLD(x) printf("%lld",x)
#define PSTR(x) printf("%s",x)
#define GARBAGE() getchar()
#define LINE() printf("\n")
#define LOOP(i,ini,fini) for(int i=ini; i<fini; i++)
#define CASES(cs,T) for(int cs=1;cs<=T;cs++)
#define CASEOUT(cs) printf("Case %d:",cs)
#define SPACE() printf(" ")
#define PB(x) push_back(x)
#define MEM(a,b) memset(a,b,sizeof(a))

#define EPS 1e-9
#define PI 2*acos(0.0)
#define MOD 1000000007

using namespace std;

int main()
{
#if 0
    FIN();
    FOUT();
#endif
    ios_base::sync_with_stdio(false);

    int t;
    string x;
    int n;
    int size;
    int temp;
    cin >> t;
    CASES(cs,t)
    {
        cin >> size;
        cin >> n;
        deque <int> q;
        cout << "Case " << cs << ":\n";
        for (int i = 0; i < n; i++)
        {
            cin >> x;
            if(x == "pushLeft" or x == "pushRight")
            {
                cin >> temp;
                if(q.size() == size)
                    cout << "The queue is full\n";
                else
                {
                    if(x == "pushLeft") cout << "Pushed in left: " << temp << "\n",q.push_front(temp);
                    else cout << "Pushed in right: " << temp << "\n",q.push_back(temp);
                }
            }
            if(x == "popLeft" or x == "popRight")
            {
                if(q.size() == 0)
                    cout << "The queue is empty\n";
                else
                {
                    if(x == "popLeft") cout << "Popped from left: " << q.front() << "\n",q.pop_front();
                    else cout << "Popped from right: " << q.back() << "\n",q.pop_back();
                }
            }
        }
    }
}
