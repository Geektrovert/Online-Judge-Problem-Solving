#include<iostream>
#include<cstdio>
#include<math.h>
using namespace std;
int main ()
{
	int t, d=1;
	cin >> t;
	while (t--)
	{
		double r1, r2, h, p, pi=acos(-1);
		cin >>r1>>r2>>h>>p;
		r1=((p/h)*(r1-r2)+r2);
		h=pi*p*(r1*r1+r2*r2+r1*r2);
		h/=3;
		printf ("Case %d: %.10lf\n", d++, h);
	}
}
