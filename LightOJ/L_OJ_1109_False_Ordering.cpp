#include <algorithm>
#include <iostream>
#include <cmath>
#include <cstring>
#include <cstdio>
#include <vector>

#define num 1001

using namespace std;

int divs[num];

struct nums
{
    int n,div;
};

void divisor()
{
    for(long long i=1; i<num; i++)
    {
            for(long long j=i; j<num; j+=i)
                divs[j]++;
    }
}

bool cmpr(nums num1, nums num2)
{
    if(num1.div==num2.div)
        return num1.n>num2.n;
    else
        return num1.div<num2.div;
}

int main()
{
    //freopen("in.c","r",stdin);
    //freopen("out.c","w",stdout);
    divisor();
    vector<nums> data;
    nums x;
    for(int i=0; i<num; i++)
    {
        x.n = i;
        x.div = divs[i];
        data.push_back(x);
    }
    sort(data.begin(),data.end(),cmpr);
    int T;
    cin >> T;
    int given;
    for(int i=0; i<T; i++)
    {
        cin >> given;
        cout << "Case " << i+1 << ": ";
        cout << data[given].n << endl;
    }
}
