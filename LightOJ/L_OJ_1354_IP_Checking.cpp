#include <iostream>
#include <cstdio>

using namespace std;

int main()
{
    //freopen("in.c","r",stdin);
    //freopen("out.c","w",stdout);
    int T;
    cin >> T;
    for(int cs=1; cs<=T; cs++)
    {
        int a,b,c,d;
        int w,x,y,z;
        bool flag_1=true, flag_2=true, flag_3=true, flag_4=true;
        scanf("%d.%d.%d.%d",&a,&b,&c,&d);
        scanf("%d.%d.%d.%d",&w,&x,&y,&z);
        //cout << a << " " << b << " " << c << " " << d << endl;
        //cout << w << " " << x << " " << y << " " << z << endl;
        while(true)
        {
            int flag=false;
            if(a%2==w%10) flag=true;
            a/=2, w/=10;
            flag_1=flag_1&flag;
            if(!a & !w) break;
        }
        while(true)
        {
            int flag=false;
            if(b%2==x%10) flag=true;
            b/=2, x/=10;
            flag_2=flag_2&flag;
            if(!b & !x) break;
        }
        while(true)
        {
            int flag=false;
            if(c%2==y%10) flag=true;
            c/=2, y/=10;
            flag_3=flag_3&flag;
            if(!c & !y) break;
        }
        while(true)
        {
            int flag=false;
            if(d%2==z%10) flag=true;
            d/=2, z/=10;
            flag_4=flag_4&flag;
            if(!d & !z) break;
        }
        if(flag_1&&flag_2&&flag_3&&flag_4)
            printf("Case %d: Yes\n",cs);
        else
            printf("Case %d: No\n",cs);
    }
}
