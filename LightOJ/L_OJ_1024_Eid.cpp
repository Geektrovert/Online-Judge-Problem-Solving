#include <algorithm>
#include <bitset>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <deque>
#include <functional>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <limits>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#define FIN() freopen("in.c","r",stdin)
#define FOUT() freopen("out.c","w",stdout)
#define IINT(x) scanf("%d",&x)
#define ID10(x) scanf("%lf",&x)
#define ILLD(x) scanf("%lld",&x)
#define ICH(x) scanf("%c",&x)
#define ISTR(x) scanf("%s",x)
#define ILN(x) scanf("%[^\n],x")
#define PINT(x) printf("%d",x)
#define PD6(x) printf("%.6lf",x)
#define PD10(x) printf("%.10lf",x)
#define PLLD(x) printf("%lld",x)
#define PCH(x) printf("%c",x)
#define PSTR(x) printf("%s",x)
#define GARBAGE() getchar()
#define LINE() printf("\n")
#define LOOP(i,ini,fini) for(int i=ini; i<fini; i++)
#define CASES(cs,T) for(int cs=1;cs<=T;cs++)
#define CASEOUT(cs) printf("Case %d:",cs)
#define SPACE() printf(" ")
#define PB(x) push_back(x)
#define MEM(a,b) memset(a,b,sizeof(a))

#define MXSZ 10001
#define EPS 1e-9
#define PI 2*acos(0.0)
#define MOD 1000000007

using namespace std;

bool prime[MXSZ];
long long facts[MXSZ];
vector<int> pprime;

void sieve()
{
    memset(prime,true, sizeof(prime));
    for(long long i=2; i<MXSZ; i++)
    {
        if(prime[i])
        {
            pprime.push_back(i);
            for(long long j=i*i; j<MXSZ; j+=i) prime[j]=false;
        }
    }
}

void factorix(int n)
{
    if(prime[n])
    {
        if(!facts[n]) facts[n]=n;
        return;
    }
    for(int i=0; pprime[i]<=n && i<pprime.size(); i++)
    {
        long long cnt=1;
        if(n%pprime[i]==0)
        {
            while(n%pprime[i]==0)
                cnt*=pprime[i],n/=pprime[i];
            facts[pprime[i]]=max(facts[pprime[i]],cnt);
        }
    }
}

string multiply( string a, long long b )
{
    int carry = 0;
    for( int i = 0; i < a.size(); i++ )
    {
        carry += (a[i] - 48) * b;
        a[i] = ( carry % 10 + 48 );
        carry /= 10;
    }
    while( carry )
    {
        a += ( carry % 10 + '0' );
        carry /= 10;
    }
    return a;
}

int main()
{
#if 0
    FIN();
    FOUT();
#endif
    ///WRITE CODE BELOW
    sieve();
    int T;
    IINT(T);
    CASES(cs,T)
    {
        MEM(facts,0);
        int n,mx=INT_MIN;
        IINT(n);
        while(n--)
        {
            int x;
            IINT(x);
            factorix(x);
            mx=max(mx,x);
        }
        //for(int i=2; i<mx; i++) PINT(i),SPACE(),PLLD(facts[i]),LINE();
        string biggie="1";
        for(int i=2; i<=mx; i++)
            if(facts[i])
                biggie=multiply(biggie,facts[i]);
        CASEOUT(cs),SPACE();
        for(int i=biggie.size()-1; i>=0; i--) PCH(biggie[i]);
        LINE();
    }
}
